"""
    Adrian Frank (4249003)
    Wolfgang Glatz (4293266)
 """
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import time
# Import data
# Note: this is deprecated is the newer tensorflow version, but it the easiest
# way to load the mnist dataset
tf.logging.set_verbosity(tf.logging.ERROR)
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('/tmp/tensorflow/mnist', one_hot=True)


# Add arguments to this function to easily run different experiments
def experiment(depth, width, lr=0.1, activation=tf.nn.relu, training_steps=5000):
    # clean any previous experiment
    tf.reset_default_graph()

    # Create the model
    x = tf.placeholder(tf.float32, [None, 784])
    y_ = tf.placeholder(tf.float32, [None, 10])

    prev_W = tf.placeholder(tf.float32, [None, None])
    prev_b = tf.placeholder(tf.float32, [None])
    prev_z = tf.placeholder(tf.float32, [None])
    act_func = activation

    for layer in range(depth+1):
        shape_W = (0,0)
        shape_b = (0)
        # input layer settings
        if layer == 0:
            shape_W = (784, width)
            shape_b = (width)
            prev_z = x
        # output layer settings
        elif layer == depth:
            shape_W = (prev_W.shape[1], 10)
            shape_b = (10)
            act_func=tf.identity
        # hidden layer settings
        else:
            shape_W = (prev_W.shape[1], width)
            shape_b = (width)
    
        prev_W = tf.get_variable('W' + str(layer), shape=shape_W, initializer=tf.initializers.random_normal(mean=0.0, stddev=0.1))
        prev_b = tf.get_variable('b' + str(layer), shape=shape_b, initializer=tf.initializers.random_normal(mean=0.0, stddev=0.1))
        prev_z = act_func(prev_z @ prev_W + prev_b)

    y = prev_z
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
    train_step = tf.train.GradientDescentOptimizer(lr).minimize(cross_entropy)

    # Train
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    train_accs = np.zeros((training_steps // 100))
    test_accs = np.zeros((training_steps // 100))

    start = time.time()
    for s in range(training_steps):
        batch_xs, batch_ys = mnist.train.next_batch(100)
        sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
        # Add code for train_accs and test_accs here!
        # b)
        if s % 100 == 0:
            index = s // 100
            acc_test, loss_train = sess.run([accuracy, cross_entropy], feed_dict={x: mnist.test.images, y_: mnist.test.labels})
            acc_train = sess.run(accuracy, feed_dict={x: mnist.train.images, y_: mnist.train.labels})
            train_accs[index] = acc_train
            test_accs[index] = acc_test

            print("In step {}, train loss is {}, train accuracy is {}, test accuracy is {}".format(s, loss_train, acc_train, acc_test))
    end = time.time()
    # Test trained model#
    final_train_acc, final_train_loss = sess.run([accuracy, cross_entropy], feed_dict={x: mnist.train.images,
                                            y_: mnist.train.labels})
    final_test_acc, final_test_loss = sess.run([accuracy, cross_entropy], feed_dict={x: mnist.test.images,
                                            y_: mnist.test.labels})
    print('final train accuracy: ', final_train_acc)
    print('final test accuracy: ', final_test_acc)
    print('training time:' ,end-start)
    
    # b)
    
    fig, ax = plt.subplots()
    x_steps = np.arange(0, training_steps, step=100)
    plt.plot(x_steps, train_accs, c='green', label='train accuracy')
    plt.plot(x_steps, test_accs, c='red', label='test accuracy')
    legend = ax.legend(loc='best', fontsize='small')

    plt.show()
    #plt.savefig(str(depth) + '_ ' + str(width) + '_' + str(lr) + '_' + str(final_train_acc) + '_' + str(final_test_acc) + '.png')
    sess.close()

# a)
# experiment(6, 90)

#c)
# lrs = np.power(10., np.arange(-8, 8))
# for lr in lrs:
#    experiment(2, 90, lr=lr)

# d)
# depths = [2, 3, 8, 12]
# widths = [10, 150, 250]
# for d in depths:
#     for w in widths:
#         experiment(d, w)

# e)
# activations = [tf.identity, tf.nn.sigmoid, tf.nn.tanh, tf.nn.softplus]
# for a in activations:
#     experiment(1, 2, activation=a)

#f)
# experiment(3, 150, activation=tf.nn.tanh, lr=0.1)
