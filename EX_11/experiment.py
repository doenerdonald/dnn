import tensorflow as tf
import dataset.dataset_loader as dl
import models.example_net as model
from dataset.dataset_loader import InferenceMode
import sys
import os
import numpy as np


class Experiment:

    def __init__(self, log_path='./log/'):

        self.__sess = tf.Session(graph=tf.get_default_graph(), config=tf.ConfigProto(allow_soft_placement=True,
                                                                                     gpu_options=tf.GPUOptions(
                                                                                         allow_growth=True),
                                                                                     log_device_placement=False))
        self.log_path=log_path
        os.makedirs(log_path, exist_ok=True)
        self.lr = 0.0001
        self.__writer = tf.summary.FileWriter(log_path, filename_suffix=".event", flush_secs=50)

        train_data_size = 50000  # eval = 10000
        self.batch_size = 100
        self.dataset_path = "dataset/challenge_data_set.npy"
        self.__iterator, self.inference_mode_var, train_size, eval_size, test_size =  dl.get_dataset_iterator(self.__sess,self.dataset_path, train_data_size, self.batch_size)

        self.__num_train_it_per_epoch = train_size // self.batch_size if train_size % self.batch_size == 0 else train_size // self.batch_size + 1  # floor division
        self.__num_eval_it_per_epoch = eval_size // self.batch_size if eval_size % self.batch_size == 0 else eval_size // self.batch_size + 1
        self.__num_test_it_per_epoch = test_size // self.batch_size if test_size % self.batch_size == 0 else test_size // self.batch_size + 1

        # self.opt_vars = self.optimizer.opt_variables

        self.loss_op, self.accuracy_op, self.predicted_label_op = model.get_example_net(self.__iterator,self.inference_mode_var)
        self.train_step_op = tf.train.AdamOptimizer(self.lr).minimize(self.loss_op)

        self.__sess.run(tf.global_variables_initializer())

    def train(self, epochs):
        """
        Trains the model for the given amount of epochs.
        After each epoch the average train accuracy, train loss, eval_loss and eval_accuracy are determined.
        Eventually, the predict_test_labels loss is calculated.
        """
        print("\n" + "-" * 40)
        print("Start training: ")
        print("-" * 40)
        self.__sess.run(
            (  self.inference_mode_var.assign(InferenceMode.TRAIN)))  # set the inference mode to train -> Iterator loads train data
        initial_loss = self.__sess.run([self.loss_op])
        print("initial loss: " + str(initial_loss))

        # Train loop
        for epoch in range(epochs):
            print("--" * 40)
            print("starting epoch " + str(epoch))
            print("--" * 40)
            self.__sess.run((self.inference_mode_var.assign(InferenceMode.TRAIN)))
            sum_of_losses = 0
            sum_of_accs = 0
            
            if epoch == epochs/2:
                self.lr /= 10

            for i in range(self.__num_train_it_per_epoch):

                _, acc, loss = self.__sess.run([self.train_step_op, self.accuracy_op, self.loss_op])
                sum_of_losses += loss
                sum_of_accs += acc
                # print('loss:\t', loss)
                # print('acc:\t', acc)

            mean_train_loss = sum_of_losses / self.__num_train_it_per_epoch
            mean_train_acc = sum_of_accs / self.__num_train_it_per_epoch
            current_step = epoch

            # logging data as Summaries to see the results in TensorBoard
            self.__log_scalar("Train/Loss", mean_train_loss, current_step)
            self.__log_scalar("Train/Accuracy", mean_train_acc, current_step)
            print("average train loss: " + str(mean_train_loss))
            print("average train accuracy: " + str(mean_train_acc))

            self.evaluate(current_step)
            sys.stdout.flush()
        return

    def evaluate(self, step):
        """
        evaluates the network
        :param step: only needed for logging
        :return: eval_acc, avg_eval_loss
        """

        self.__sess.run((self.inference_mode_var.assign(InferenceMode.EVAL)))
        sum_of_losses = 0
        sum_of_accs = 0

        for j in range(self.__num_eval_it_per_epoch):
            acc, loss, = self.__sess.run((self.accuracy_op, self.loss_op))
            sum_of_losses += loss
            sum_of_accs += acc

        mean_eval_loss = sum_of_losses / self.__num_eval_it_per_epoch
        mean_eval_acc = sum_of_accs / self.__num_eval_it_per_epoch

        # LogSummaries
        self.__log_scalar("Evaluation/Accuracy", mean_eval_acc, step)
        self.__log_scalar("Evaluation/Loss", mean_eval_loss, step)

        # print stats
        print("average evaluation loss: {0:.6f}".format(mean_eval_loss))
        print("evaluation set accuracy: {0:.4f}".format(mean_eval_acc))

        return mean_eval_acc, mean_eval_loss

    def predict_test_labels(self):

        self.__sess.run((self.inference_mode_var.assign(InferenceMode.TEST)))
        predicted_labels=[]

        for j in range(self.__num_test_it_per_epoch):
            label_batch = self.__sess.run((self.predicted_label_op))
            predicted_labels.extend(label_batch)
        label_path=self.log_path+"predicted_labels.csv"
        self.saveKaggleFile(label_path,predicted_labels)
        print("saved predicted test set labels to "+label_path)

    def __log_scalar(self, tag, value, step):
        """
        logs a scalar outside of the graph

        :param tag:  name of the scalar
        :param value: a scalar
        :param step:  training iteration
        source: https://gist.github.com/gyglim/1f8dfb1b5c82627ae3efcfbbadb9f514
        """
        summary = tf.Summary(value=[tf.Summary.Value(tag=tag, simple_value=value)])
        # is not more expensive as creating summary in graph and logging it later
        self.__writer.add_summary(summary, step)

    def saveKaggleFile(self, filename, predictions):
        predictions = np.reshape(predictions, (-1, 1))
        header = "Id,Category"
        indizes = range(len(predictions))
        predictions = np.insert(predictions, 0, indizes, 1)
        np.savetxt(filename, predictions, delimiter=",", header=header)  # '''

if __name__ == "__main__":
    import time

    example = Experiment(log_path='./log/%d/' % int(time.time()))
    example.train(100)
    example.predict_test_labels()

# competition link
#https://www.kaggle.com/t/166639a48b2642b5afa3192d2e773b25
