import numpy as np
from typing import List, Dict, Union, Set

all_tensors = []
all_variables = []


class Tensor:
    """
    The base class for all tensors. Should never be constructed by itself since it does not have its shape set in its
    constructor. Instead construct objects from its derived classes.
    """
    def __init__(self):
        self._inputs = []
        self._outputs = []
        all_tensors.append(self)

    def _register_output(self, tensor):
        self._outputs.append(tensor)

    def _register_with_inputs(self):
        for tensor in self._inputs:
            tensor._register_output(self)

    @property
    def shape(self):
        return self._shape

    def _forward_pass(self, value_table) -> np.ndarray:
        """
        Compute the output of this tensor during a forward pass
        To get the values of its input tensors it needs to get them from its input list: 'self._inputs[n]'
        and then look up the value in the value_table: value_table[self._inputs[n]]
        :param value_table: dictionary of tensor values computed so far
        :return: value for this tensor
        """
        pass


    def _backward_pass(self, tensor, grad_table, value_table) -> np.ndarray:
        """
        Compute the output of this tensor during the backward pass
        The incoming gradient can be retrieved as grad_table[self]
        :param tensor: specifies for which of its inputs the gradient should be computed
        :param grad_table: dictionary of the values from the backward pass so far
        :param value_table: dictionary of the values from the forward pass
        :return: value of the gradient for the specified tensor from this tensor
        """
        pass

    def __add__(self, other):
        """
        operator overloading for the '+' operator
        """
        return Add(self, other)

    def __matmul__(self, other):
        """
        operator overloading for the '@' operator
        """
        return MatMul(self, other)


class Input(Tensor):
    """
    Dummy class used for feeding values into the network. Must have a fixed shape including batch size.
    """
    def __init__(self, shape):
        super().__init__()
        self._shape = shape


class Variable(Tensor):
    """
    Tensor that saves values between different runs. Gets a default initialization depending on its shape:
    < 2D: zeros
    >= 2D: random uniform with limits +- sqrt(6/(n_in + n_out)) (Glorot-initialization)
    """
    def __init__(self, shape):
        super().__init__()
        self._shape = shape
        # give a default initializer
        if len(shape) < 2:
            self._value = np.zeros(shape)
        else:
            input_size = np.sum(shape[:-1])
            output_size = shape[-1]
            scale = np.sqrt(6/(input_size + output_size))
            self._value = np.random.uniform(-scale, scale, shape)
        all_variables.append(self)

    def update_variable(self, value):
        if value.shape != self.shape:
            raise ValueError("This Variable has shape {}, new value has shape {}".format(self.shape, value.shape))
        self._value = value

    def _forward_pass(self, value_table):
        return self._value


class Add(Tensor):
    """
    Tensor operation to facilitate addition. Does broadcasting between the two inputs, e.g. adding a 1-D bias vector to
    a 2-D matrix consisting of a batch of activations.
    """
    def __init__(self, addend1, addend2):
        super().__init__()
        b = np.broadcast(np.zeros(addend1.shape), np.zeros(addend2.shape))
        self._shape = b.shape
        self._inputs = [addend1, addend2]
        self._register_with_inputs()

    def _forward_pass(self, value_table):
        addend1_value = value_table[self._inputs[0]]
        addend2_value = value_table[self._inputs[1]]
        return addend1_value + addend2_value

    def _backward_pass(self, tensor, grad_table, value_table):
        if tensor.shape != self.shape:
            tensor_shape = tensor.shape
            while len(tensor_shape) < len(self.shape):
                tensor_shape = (1,) + tensor_shape
            different_dims = np.array(tensor_shape) != np.array(self.shape)
            sum_dims = np.arange(len(self.shape))[different_dims]
            ret = np.sum(grad_table[self],axis=tuple(sum_dims))
            return ret
        else:
            return grad_table[self]


class MatMul(Tensor):
    """
    Matrix multiplication operation tensor, both inputs must be matrices, e.g. a weight matrix and a matrix of
    activations over a batch.
    """
    def __init__(self, factor1, factor2):
        super().__init__()
        if len(factor1.shape) != 2 or len(factor2.shape) != 2:
            raise ValueError("Only Matrices/2D-Arrays supported")
        s1 = factor1.shape
        s2 = factor2.shape
        if s1[1] != s2[0]:
            raise ValueError("Dimensions for matrix multiplication not compatible: {} and {}".format(s1, s2))
        self._shape = (s1[0], s2[1])
        self._inputs = [factor1, factor2]
        self._register_with_inputs()

    def _forward_pass(self, value_table):
        factor1_value = value_table[self._inputs[0]]
        factor2_value = value_table[self._inputs[1]]
        return np.matmul(factor1_value, factor2_value)

    def _backward_pass(self, tensor, grad_table, value_table):
        """
        Compute the output of this tensor during the backward pass
        The incoming gradient can be retrieved as grad_table[self]
        :param tensor: specifies for which of its inputs the gradient should be computed
        :param grad_table: dictionary of the values from the backward pass so far
        :param value_table: dictionary of the values from the forward pass
        :return: value of the gradient for the specified tensor from this tensor
        """
        idx = self._inputs.index(tensor)

        if idx == 0:
            #TODO gradient w.r.t. to the first argument
            grad_in = grad_table[self]
            return grad_in @ value_table[self._inputs[1]].T
        elif idx == 1:
            #TODO gradient w.r.t. to the second argument
            grad_in = grad_table[self]
            return value_table[self._inputs[0]].T @ grad_in
        else:
           ValueError("Given Tensor is not an input to this one")
        pass


class ReLU(Tensor):
    """
    A tensor to apply the ReLU (Rectified Linear Unit, ReLU(x) = max(x, 0)) activation function elementwise to its
    input tensor.
    """
    def __init__(self, tensor_in):
        super().__init__()
        self._shape = tensor_in.shape
        self._inputs = [tensor_in]
        self._register_with_inputs()

    def _forward_pass(self, value_table):
        inp_value = value_table[self._inputs[0]]
        return np.maximum(inp_value, 0.)

    def _backward_pass(self, tensor, grad_table, value_table):
        grad_in = grad_table[self]
        forward_value = value_table[self]
        return (forward_value > 0) * grad_in


class SoftmaxCrossEntropy(Tensor):
    """
    Computes the softmax cross-entropy loss from the given pre-activations (logits) and labels. The labels must be
    one-hot encoded and the output is a single loss value for every line of input.
    """
    def __init__(self, logits, labels):
        super().__init__()
        if logits.shape != labels.shape:
            ValueError("Logits must have the same shape as the labels: {} and {}".format(logits.shape, labels.shape))
        self._shape = logits.shape[:-1]
        self._inputs = [logits, labels]
        self._register_with_inputs()

    def _forward_pass(self, value_table):
        logit_value = value_table[self._inputs[0]]
        label_value = value_table[self._inputs[1]]
        logits_stabilized = (logit_value.T - np.amax(logit_value, axis=-1)).T
        return np.log(np.sum(np.exp(logits_stabilized), axis=-1)) - np.sum(logits_stabilized*label_value, axis=-1)

    def _backward_pass(self, tensor, grad_table, value_table):
        idx = self._inputs.index(tensor)
        if idx == 0:
            grad_in = grad_table[self]
            logit_value = value_table[self._inputs[0]]
            label_value = value_table[self._inputs[1]]
            logits_stabilized = (logit_value.T - np.amax(logit_value, axis=-1)).T
            exp_logits = np.exp(logits_stabilized)
            softmax_grad = (exp_logits.T/np.sum(exp_logits, axis=-1)).T - label_value
            return (softmax_grad.T * grad_in).T
        else:
            raise ValueError("Only does gradients for the logit input")


class CheckPrediction(Tensor):
    """
    Checks examples were classified correctly from the pre-activations and correct labels. Outputs a 1 or a 0 for each
    example depending on whether it was classified correctly. Does not have a backward_pass since its output changes
    discontinuously.
    """
    def __init__(self, input1, input2):
        super().__init__()
        if input1.shape != input2.shape:
            raise ValueError("Inputs must have same shape: {} and {}".format(input1.shape, input2.shape))
        self._shape = input1.shape[:-1]
        self._inputs = [input1, input2]
        self._register_with_inputs()

    def _forward_pass(self, value_table):
        in1_value = value_table[self._inputs[0]]
        in2_value = value_table[self._inputs[1]]
        return np.equal(np.argmax(in1_value, axis=-1), np.argmax(in2_value, axis=-1), dtype=np.float32)


class ReduceMean(Tensor):
    """
    Get the average over all the values in a tensor. Mostly for use with SoftmaxCrossEntropy and CorrectPrediction.
    """
    def __init__(self, tensor_in):
        super().__init__()
        self._shape = ()
        self._inputs = [tensor_in]
        self._register_with_inputs()

    def _forward_pass(self, value_table):
        inp_value = value_table[self._inputs[0]]
        return np.mean(inp_value)

    def _backward_pass(self, tensor, grad_table, value_table):
        grad_in = grad_table[self]
        size = np.prod(tensor.shape)
        return grad_in/size * np.ones(tensor.shape)


def _add_to_forward_eval_list(tensor: Tensor, eval_list: List[Tensor], eval_set: Set[Tensor]) -> None:
    """
    helper function for _make_forward_eval_list
    """
    for input_tensor in tensor._inputs:
        if input_tensor not in eval_set:
            _add_to_forward_eval_list(input_tensor, eval_list, eval_set)
    eval_set.add(tensor)
    eval_list.append(tensor)


def _make_forward_eval_list(targets: List[Tensor]) -> List[Tensor]:
    """
    Make a list of the tensors that need to be evaluated in order to get values for the target tensors. The order of the
    list must make sure that each tensor has all its inputs available when it is evaluated.
    """
    eval_list = []
    eval_set = set()
    for target_tensor in targets:
        _add_to_forward_eval_list(target_tensor, eval_list, eval_set)
    return eval_list


def do_forward_pass(targets: Union[Tensor, List[Tensor]], initial_values: Dict[Tensor, np.ndarray],
                    return_all_values: bool = False) -> Union[np.ndarray, List[np.ndarray], Dict[Tensor, np.ndarray]]:
    """
    Performs a forward pass through the network, evaluating all the tensors required to get a value for the target(s).
    :param targets: either a single Tensor or a list of Tensors, the desired output.
    :param initial_values: values for all the Input tensors
    :param return_all_values: whether to return just the desired target values or values for all computed tensors
    :return: Either the values for the desired target tensor or a dictionary with the values for all computed tensors
    """
    if type(targets) is not list:
        targets = [targets]

    for tensor, value in initial_values.items():
        if tensor.shape != value.shape:
            raise ValueError("Provided value with shape {} doesn't match shape of tensor {}"
                             .format(value.shape, tensor.shape))

    eval_list = _make_forward_eval_list(targets)
    value_table = initial_values
    for tensor in eval_list:
        if tensor not in value_table:
            value_table[tensor] = tensor._forward_pass(value_table)

    if return_all_values:
        return value_table
    elif type(targets) is not list:
        return value_table[targets]
    else:
        return [value_table[t] for t in targets]


def _add_to_backward_eval_list(tensor: Tensor, eval_list: List[Tensor],
                               eval_set: Set[Tensor], limiting_set: Set[Tensor]) -> None:
    """
    Helper function for _make_backward_eval_list.
    """
    for output_tensor in tensor._outputs:
        if output_tensor not in eval_set and output_tensor in limiting_set:
            _add_to_backward_eval_list(output_tensor, eval_list, eval_set, limiting_set)
    eval_set.add(tensor)
    eval_list.append(tensor)


def _make_backward_eval_list(objective: Tensor, targets: List[Tensor]) -> List[Tensor]:
    """
    Make a list of all the tensors that need to be evaluated to get the gradients of the objective with respect to the
    targets. Assuming the values from the forward pass are available, the order of the list must ensure that all
    necessary values are available when performing the _backward_pass for each tensor.
    :param objective: The Tensor for which the gradient should be computed
    :param targets: The tensors for which we want the gradients
    :return: A list of tensors in the order they should be evaluated and a set with the same tensors
    """
    objective_tree = set(_make_forward_eval_list([objective]))
    eval_list = []
    eval_set = set()
    for target_tensor in targets:
        _add_to_backward_eval_list(target_tensor, eval_list, eval_set, objective_tree)

    assert(eval_list[0] == objective)
    eval_list.pop(0)
    return eval_list, eval_set


def do_backward_pass(objective: Tensor, targets: List[Tensor],
                     value_table: Dict[Tensor, np.ndarray]):
    """
    Perform a pass of backpropagation in the network. This should be called after a forward pass since the intermediate
    values from the forward pass are required for some of the backward pass operations.
    :param objective: The Tensor for which the gradient should be computed, must be a scalar (shape=())
    :param targets: The tensors for which we want the gradients
    :param value_table: dictionary of values computed in the forward pass
    :return: A list of gradients corresponding to the target tensors
    """

    if objective.shape != ():
        raise ValueError("Only scalar objectives are supported")
    if objective not in value_table:
        raise ValueError("The objective tensor needs to have been evaluated in the forward pass")
    eval_list, eval_set = _make_backward_eval_list(objective, targets)
    
    grad_table = {objective: np.array(1)}
    for tensor in eval_list:   
		#TODO Implement the backward propagation algorithm (p. 67)
        # grad_table is dictionary which contains mappings from Tensors to their gradient values
        # eval_list is a list of the nodes to be evaluated in the backward pass, ordered so that each gradient value
	    #           has been computed before it is needed by another Tensor
        # eval_set is a set version of eval_list, for easier checking with "if some_tensor in eval_set:"
        # Tensors keep track of both their inputs and outputs via the '_inputs' and '_outputs' attributes

        new_entry = 0
        for output in tensor._outputs:
            if output in eval_set:
                new_entry = new_entry + output._backward_pass(tensor, grad_table, value_table)
            else:
                continue
        grad_table[tensor] = new_entry

    return [grad_table[t] for t in targets]
