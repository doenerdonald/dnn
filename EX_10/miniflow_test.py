import numpy as np
import miniflow as mf
import mnist

# Load the raw MNIST
X_train, y_train = mnist.read(dataset='training')
X_test, y_test = mnist.read(dataset='testing')

# As a sanity check, we print out the size of the training and test data.
print('Training data shape: ', X_train.shape)
print('Training labels shape: ', y_train.shape)
print('Test data shape: ', X_test.shape)
print('Test labels shape: ', y_test.shape)

# Reshape the image data into rows
# Datatype float allows you to subtract images (is otherwise uint8)
X_train = np.reshape(X_train, (X_train.shape[0], -1)).astype('float')
X_test = np.reshape(X_test, (X_test.shape[0], -1)).astype('float')
print(X_train.shape, X_test.shape)

# One-hot encoding the labels
y_train_ = np.zeros((y_train.size, 10))
y_train_[np.arange(y_train.size), y_train] = 1.
y_train = y_train_

y_test_ = np.zeros((y_test.size, 10))
y_test_[np.arange(y_test.size), y_test] = 1.
y_test = y_test_


# normalizing the input to make training easier
X_mean = np.mean(X_train)
X_stddev = np.sqrt(np.var(X_train)+1e-4)

X_train = (X_train - X_mean)/X_stddev
X_test = (X_test - X_mean)/X_stddev


def batch(num):
    idx = np.random.randint(60000, size=num)
    return X_train[idx, :], y_train[idx]


# Parameters
training_steps = 10000
batch_size = 128
lr = 0.1

# Defining the network
mnist_inputsize = X_train.shape[-1]
x = mf.Input((batch_size, mnist_inputsize))
y = mf.Input((batch_size, 10))

#TODO build a Network with miniflow
hidden_layer_size = 50
W1 = mf.Variable((x.shape[1], hidden_layer_size))
W2 = mf.Variable((hidden_layer_size, hidden_layer_size))
W3 = mf.Variable((hidden_layer_size, y.shape[1]))
b1 = mf.Variable((hidden_layer_size,))
b2 = mf.Variable((hidden_layer_size,))
b3 = mf.Variable((y.shape[1],))


z1 = mf.Add(mf.MatMul(x, W1), b1)
u1 = mf.ReLU(z1)
z2 = mf.Add(mf.MatMul(u1, W2), b2)
u2 = mf.ReLU(z2)
z3 = mf.Add(mf.MatMul(u2, W3), b3)

loss = mf.ReduceMean(mf.SoftmaxCrossEntropy(z3, y)) # add Loss Tensor here
accuracy = mf.ReduceMean(mf.CheckPrediction(z3, y)) # add accuracy Tensor here


def evaluate():
    """
    Does evaluation on the test set in batches due to fixed input size.
    """
    test_len = X_test.shape[0]
    batches = int(np.floor(test_len/batch_size))
    acc = 0
    for i in range(batches):
        i_st = i*batch_size
        i_en = (i+1)*batch_size
        x_test_batch = X_test[i_st:i_en]
        y_test_batch = y_test[i_st:i_en]
        [a] = mf.do_forward_pass(accuracy, {x: x_test_batch, y: y_test_batch})
        acc += a
    return acc / batches


for step in range(training_steps):
    X_batch, y_batch = batch(batch_size)
    value_table = mf.do_forward_pass(loss, {x: X_batch, y: y_batch}, True)
    #print(mf.all_variables[0]._inputs)
    grads = mf.do_backward_pass(loss, mf.all_variables, value_table)
    #input('wait')
    for var, grad in zip(mf.all_variables, grads):
        var.update_variable(value_table[var] - lr*grad)

    if step % 500 == 0:
        print("step: {}, loss = {}, test accuracy = {}".format(step, value_table[loss], evaluate()))

    # reduce learning rate at half and 3/4 of training steps
    if step == training_steps/2:
        lr /= 10

print()

# evaluate
print("Test Accuracy: {}".format(evaluate()))
