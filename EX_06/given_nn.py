#!/usr/bin/python3
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data


# Import data
# Note: this is deprecated is the newer tensorflow version, but it the easiest
# way to load the mnist dataset
mnist = input_data.read_data_sets('/tmp/tensorflow/mnist', one_hot=True)

# Create the model
# adds a placeholder for the current image to the computational graph. It has to be defined (feeded) for each session.run call (see line 39)
# the dimension None means, that this dimension is not defined, thus it can have any value.
x = tf.placeholder(tf.float32, [None, 784])
# adds an unitialized Matrix to the computational graph with an initializer that defines how to initialize it.
# other intializers: e.g: tf.initializers.random_normal or tf.initializers.random_uniform
W = tf.get_variable('W', shape=(784,10), initializer=tf.zeros_initializer)  
# adds an unitialized Matrix to the computational graph.
b = tf.get_variable('b', shape=(10), initializer=tf.zeros_initializer)

# adds an operation  to thecomputational graph that outputs a tensor.
y = tf.matmul(x, W) + b

# Define loss and optimizer
# adds a placeholder for the current label to the computational graph. It has to be defined (feeded) for each session.run call (see line 39)
y_ = tf.placeholder(tf.float32, [None, 10])

# Accuracy for testing purposes
# adds two operations to the computational graph which output a tensor each.
correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# tf.nn.softmax_cross_entropy_with_logits is a numerically stabilized softmax-
# cross-entropy, the logits are the preactivations calculated by the net
cross_entropy = tf.reduce_mean(
  tf.nn.softmax_cross_entropy_with_logits_v2(labels=y_, logits=y))
# adds an operations to the computational graph that performs a gradient descent training step.
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

# create a session to execute the defined graph
sess = tf.Session()
# initialize all variables.
sess.run(tf.global_variables_initializer())


# Train
# this is standard python code
for s in range(5000):
  batch_xs, batch_ys = mnist.train.next_batch(100)
  # performs an inferenz over a subgraph of the defined computational graph.
  # all nodes "train_step" depends on are processed.
  # to complete the subgraph the placeholders have to be defined.
  sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
  if s % 100 == 0:
    # make an inferenz over a subgraph of the defined computational graph.
    # all nodes "accuracy" and "cross_entropy" depend on are processed.
    # to complete the subgraph the placeholders have to be defined.
    acc, loss = sess.run([accuracy, cross_entropy],
                         feed_dict={x: mnist.test.images, y_: mnist.test.labels})
    print("At Step {}: Accuracy = {}, Loss = {}".format(s, acc, loss))

# Test trained model
print(sess.run(accuracy, feed_dict={x: mnist.test.images,
                                    y_: mnist.test.labels}))
