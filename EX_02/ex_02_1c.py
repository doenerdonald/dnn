from scipy.stats import multivariate_normal

x1 = [0.2, 0.3]
x2 = [0.45, 0.65]
x3 = [0.6, 0.8]

mean_cat = [0.3, 0.5]
mean_dog = [0.6, 0.8]

cov_cat = [[0.04, 0.03], [0.03, 0.04]]
cov_dog = [[0.016, 0.01], [0.01, 0.016]]

print('CAT')
print(multivariate_normal.pdf([x1, x2, x3], mean_cat, cov_cat))
print('DOG')
print(multivariate_normal.pdf([x1, x2, x3], mean_dog, cov_dog))