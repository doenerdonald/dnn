#!/usr/bin/python3
# -*- coding: utf-8 -*-
import csv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

# (a)

def load_data(filename):
    """should load the data from the given file, returning a matrix for X and a vector for y"""
    Xy = np.loadtxt(filename, delimiter=',')
    X = Xy[:,0:2]
    y = Xy[:,2]
    return (X,y)


X,y = load_data('data_train.csv')

# extend the data in order to add a bias term to the dot product with theta
m = len(y)
X = np.column_stack([np.ones(m), X])

# print(X)
# print(y)

## now plot the data
def plot_points(grid=None, show_test=False):
  fig, ax = plt.subplots(figsize=(9,7))
  if grid:
    all_x, all_y, all_predictions = grid
    contour_lines = ax.contourf(all_x, all_y, all_predictions, 25, cmap='RdBu')
    color_legend = fig.colorbar(contour_lines, orientation='horizontal', shrink=0.5)
    color_legend.set_label("$P(y = admission)$")
    color_legend.set_ticks([0, .25, .5, .75, 1])
  class_colors = ['#EC7B7B', '#0F7B0B']
  class_legends = ['no admission', 'admission']

  colors = [class_colors[int(label)] for label in y]
  plt.scatter(X[:, 1], X[:, 2], c=colors)
  plt.xticks(np.arange(min(X[:, 1]), max(X[:, 1])+0.5, step=0.5))
  plt.yticks(np.arange(int(min(X[:, 2])), max(X[:, 2])+0.5, step=0.5))
  plt.xlabel('Exam 1')
  plt.ylabel('Exam 2')
  plt.title('Exam scores and Admission result', loc='left')
  rects = [mpatches.Rectangle((0,0), 1, 1, color=c, label=class_legends[idx]) for idx, c in enumerate(class_colors)]
  if show_test:
    test_colors = [class_colors[int(label)] for label in y_test]
    plt.scatter(X_test[:, 1], X_test[:, 2], c=test_colors, edgecolors='white')
    rects.append(mpatches.Rectangle((0,0), 1, 1, fill=False, edgecolor='white', label='test points'))
  plt.legend(handles=rects, bbox_to_anchor=(0., 1.02, 1., .102), loc=4,
            ncol=1, borderaxespad=0., facecolor='#cecece')
  plt.show()

plot_points()

# (b)
def sigmoid(x):
    return 1 / (1 + np.exp(- x))


# # (f) move this section to before (c) when you use it
def polynomial_extension(X, degree):
    polynomial_X = X
    iterations = 0
    for d_i in range(degree + 1):
        for d_j in range(degree + 1 - d_i):
          if (d_i == 0 and d_j == 0) or (d_i == 0 and d_j == 1) or (d_i == 1 and d_j == 0):
            continue
          polynmial_rep = (X[:, 1]**d_j) * (X[:, 2] ** (d_i))
          polynomial_X = np.concatenate((polynomial_X, np.vstack(polynmial_rep)), axis=1)
          iterations += 1
    return polynomial_X




# # (c)
def cost(theta):
    """compute the cost function from the parameters theta"""
    
    predicted_prob = sigmoid(X @ theta)
    left_term  = y * np.log(predicted_prob)
    right_term = (1 - y) * np.log(1 - predicted_prob)
    return - (right_term + left_term).sum()

def gradient(theta):
#     """compute the derivative of the cost function with respect to theta"""
    return X.T @ (sigmoid(X @ theta) - y)

# (d)
def gradient_descent(gradf, theta0, lr, steps, costf, verbose=True):
    """
    Args:
      gradf: gradient of the cost function
      theta0: initail value for the parameters theta
      lr: learing rate
      steps: total number of iterations to perform
      costf: cost function (only needed for debugging/outputting intermediate results)
    returns the final value for theta
    """

    curr_weights = theta0
    for step in range(steps):
      if verbose:
        print('Current training loss in iteration', step, ':' , costf(curr_weights))
      curr_gradient = gradf(curr_weights)
      curr_weights = curr_weights - lr * curr_gradient
    return curr_weights

def get_accuracy(pred):
  pred = np.array(pred > 0.5, dtype=int)
  TP = 0
  TN = 0
  for idx in range(len(pred)):
    if pred[idx] == y_test[idx]:
      if pred[idx] == 0:
        TN += 1
      else:
        TP += 1
    else:
      pass
  return (TP+TN) / len(y_test)

# # (e)
# # use these parameters for training the model
theta_trained = gradient_descent(gradient, np.zeros(len(X[0,:])), 1e-4, 100000, cost, verbose=True)
print("Trained theta values: ", theta_trained)

X_test, y_test = load_data("data_test.csv")
X_test = np.column_stack([np.ones(len(y_test)), X_test])

# # evaluate the accuracy of the model on the test set
prediction = sigmoid(X_test @ theta_trained)
accuracy = get_accuracy(prediction)
print('Accuracy after training is: ', accuracy)


# # add the decision boundary to the plot
all_x, all_y = np.mgrid[2.5:11:.5, 2.5:11:.5]
grid = np.c_[all_x.ravel(), all_y.ravel()]
grid = np.column_stack([np.ones(grid.shape[0]), grid])
all_predictions = sigmoid(grid @ theta_trained).reshape(all_x.shape)

plot_points((all_x, all_y, all_predictions))

# f
degree = 2
X = polynomial_extension(X, degree)
theta_trained = gradient_descent(gradient, np.zeros(len(X[0,:])), 1e-4, 100000, cost, verbose=False)
print("Trained theta values with polynomial of degree ", degree,  ": ", theta_trained)

X_test, y_test = load_data("data_test.csv")
X_test = np.column_stack([np.ones(len(y_test)), X_test])
X_test = polynomial_extension(X_test, degree)

prediction = sigmoid(X_test @ theta_trained)
accuracy = get_accuracy(prediction)
print('Accuracy after training with polynomial of degree', degree, 'is: ', accuracy)

all_x, all_y = np.mgrid[2.5:11:.5, 2.5:11:.5]
grid = np.c_[all_x.ravel(), all_y.ravel()]
grid = np.column_stack([np.ones(grid.shape[0]), grid])
grid = polynomial_extension(grid, degree)
all_predictions = sigmoid(grid @ theta_trained).reshape(all_x.shape)

plot_points((all_x, all_y, all_predictions), show_test=True)
