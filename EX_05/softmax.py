#!/usr/bin/python3
import numpy as np

#
# (b)
#
# compute the softmax for the preactivations a.
# a can eithe be a vector or a matrix. If a is a matrix, calculate the
# softmax over each row
#

def softmax(a):
    #TODONE
    e_a = np.exp(a - np.max(a,axis=1,keepdims=True))
    s_a = e_a / np.sum(e_a,axis=1,keepdims=True)
    return s_a

#
# (c)
#
# compute the softmax-cossentropy between the preactivations a and the
# correct class y.
# y is an integer indicating the correct class, 0 <= y < np.size(a, axis=-1).
# a and y can eithe be a vector and an int or a matrix and a vector. If a is
# a matrix, calculate the softmax-crossentropy of each row with the
# corresponding element of the vector
#
def softmax_crossentropy(a, y):
    #TODONE
    y_one_hot = np.eye(classes)[y[:,0]]
    ce_loss = -np.sum(y_one_hot*np.log(softmax(a)),axis=1)
    return ce_loss
#
# (d)
#    
# compute the gradient of the softmax-cossentropy between the
# preactivations a and the correct class y with respect to the preactivations
# a.
# y is an integer indicating the correct class, 0 <= y < np.size(a, axis=-1).
# a and y can eithe be a vector and an int or a matrix and a vector. If a is
# a matrix, calculate the gradient of the softmax-crossentropy of each row
# with the corresponding element of the vector
#
def grad_softmax_crossentropy(a, y):
    y_one_hot = np.eye(classes)[y[:,0]]
    grad = softmax(a) - y_one_hot
    return grad
    
#
# (e)
#

batch_size = 21
classes = 10
a_init = np.random.randint(1,100,(batch_size,classes)).astype("float")
y_init = np.random.randint(0,np.size(a_init,axis=1),(batch_size,1))

eps = 1e-5
a_eps = a_init.copy()
np.diag(grad_softmax_crossentropy(a_init, y_init)[:, y_init.flatten()])

for i, o in zip(y_init.max(axis=1), range(y_init.shape[0])):
    a_eps[o, i] += eps
    
print(softmax_crossentropy(a_eps+eps,y_init)-softmax_crossentropy(a_init,y_init))    

# To compute the numerical gradient at a point (a,y), for component i compute
# '(ce(a+da,y)-ce(a,y))/e' where 'da[i] = e' and the other entries of 'da' are
# zero and e is a small number, e.g. 0.0001 (i.e. use the finite differences
# method for each component of the gradient separately).

# implemented correctly, the difference between analytical and numerical
# gradient should be of the same magnitude as e
