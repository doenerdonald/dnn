import numpy as np
alphabet = {
    'A': 0.0558,
    'Ä': 0.0054,
    'B': 0.0196,
    'C': 0.0316,
    'D': 0.0498,
    'E':  0.1693,
    'F': 0.0149,
    'G': 0.0302,
    'H': 0.0498,
    'I': 0.0802,
    'J': 0.0024,
    'K': 0.0132,
    'L': 0.0360,
    'M': 0.0255,
    'N':  0.1053,
    'O': 0.0224,
    'Ö': 0.0030,
    'P': 0.0067,
    'Q': 0.0002,
    'R': 0.0689,
    'ß': 0.0037,
    'S': 0.0642,
    'T': 0.0579,
    'U': 0.0383,
    'Ü': 0.0065,
    'V': 0.0084,
    'W': 0.0178,
    'X': 0.0005,
    'Y': 0.0005,
    'Z': 0.0121
  }
summed = 0
for value in alphabet.values():
      summed = summed + 1/30  *np.log2(value)

print(summed)