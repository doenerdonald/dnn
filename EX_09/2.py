import numpy as np

x = np.array([1, 2, 3, 4])
y_hat = np.array([1, 0, 1])

W1 = np.array([
  [-0.7, 0.2, 0.1, 0.3],
  [-0.5, 0.25, 1, 0.2],
  [-0.3, 0.4, 0.3, 0],
  [-0.5, 0.5, 0, 0.5]
])

W2 = np.array([
  [0.1, 0, 0],
  [0.25, 0.3, -0.3],
  [0.5, 0.9, 0],
  [0.4, 0.1, -0.5]
])

u1 = x @ W1
print('------u1-------')
print(u1)
#input('wait')

z1 = u1
z1[z1 < 0] = 0
print('------z1-------')
print(z1)
#input('wait')

u2 = z1 @ W2
print('------u2-------')
print(u2)

y = u2
y[y < 0] = 0
print('------y-------')
print(y)

psi1 = y - y_hat
print('------psi1-------')
print(psi1)


psi2 = np.linalg.norm(psi1, ord=2)
print('------psi2-------')
print(psi2)

psi3 = psi2 ** 2
print('------psi3-------')
print(psi3)

L = psi3 * 1/2
print('------L-------')
print(L)

print()
print('END FORWARDPASS')
print()

dLdy = y - y_hat
print('------dLdy-------')
print(dLdy)


dydu2 = u2
dydu2[dydu2 < 0] = 0
dydu2[dydu2 > 0] = 1

print('------dydu2-------')
print(dydu2)

du2w2 = z1

print('------du2w2-------')
print(du2w2)

dLdu2 = (dLdy * dydu2)
print('------dLdu2-------')
print(dLdu2)

dLdw2 = np.outer(du2w2, dLdu2)
print('------dLdw2-------')
print(dLdw2)

dLdz1 = dLdu2 @ W2.T
print('------dLdz1-------')
print(dLdz1)


dz1du1 = u1
dz1du1[dz1du1 < 0] = 0
dz1du1[dz1du1 > 0] = 1
print('------dz1du1-------')
print(dz1du1)

dLdu1 = dLdz1 * dz1du1
print('------dLdu1-------')
print(dLdu1)

dLdw1 = np.outer(x, dLdu1)
print('------dLdw1-------')
print(dLdw1)
