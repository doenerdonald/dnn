"""
    Adrian Frank (4249003)
    Wolfgang Glatz (4293266)
 """

import tensorflow as tf
import numpy as np

tf.reset_default_graph()

# Create the model
x = tf.placeholder(tf.float32, [1, 16], name='x')
W1 = tf.placeholder(tf.float32, [16, 10], name='W1')
# u1 shape = 1 x 10
# z1 shape = 1 x 10
W2 = tf.placeholder(tf.float32, [10, 32], name='W2')
# u2 shape = 1 x 32
# z2 shape = 1 x 32
#y = tf.placeholder(tf.float32, [1, 32], name='y')
y_hat = tf.placeholder(tf.float32, [1, 32], name='y_hat')
#L = tf.placeholder(tf.float32, [], name='L')

u1 = tf.matmul(x, W1, name='u1')
z1 = tf.nn.relu(u1, name='z1')
u2 = tf.matmul(z1, W2, name='u2')
y = tf.nn.relu(u2, name='y')
L = tf.nn.l2_loss(tf.subtract(y, y_hat), name='L')

tf.gradients(L, [W1, W2, x], name='')
tf.summary.FileWriter('.', tf.get_default_graph())
tf.summary.histogram('name',  W2)
tf.summary.histogram('name',  W1)
tf.summary.histogram('name',  x)
tf.summary.histogram('name',  y)
tf.summary.histogram('name',  y_hat)
tf.summary.histogram('name',  L)
tf.summary.histogram('name',  u1)
tf.summary.histogram('name',  u2)
tf.summary.histogram('name',  z1)



# y_ = tf.placeholder(tf.float32, [None, 10])

# prev_W = tf.placeholder(tf.float32, [None, None])
# prev_b = tf.placeholder(tf.float32, [None])
# prev_z = tf.placeholder(tf.float32, [None])
# act_func = activation

# for layer in range(depth+1):
#     shape_W = (0,0)
#     shape_b = (0)
#     # input layer settings
#     if layer == 0:
#         shape_W = (784, width)
#         shape_b = (width)
#         prev_z = x
#     # output layer settings
#     elif layer == depth:
#         shape_W = (prev_W.shape[1], 10)
#         shape_b = (10)
#         act_func=tf.identity
#     # hidden layer settings
#     else:
#         shape_W = (prev_W.shape[1], width)
#         shape_b = (width)

#     prev_W = tf.get_variable('W' + str(layer), shape=shape_W, initializer=tf.initializers.random_normal(mean=0.0, stddev=0.1))
#     prev_b = tf.get_variable('b' + str(layer), shape=shape_b, initializer=tf.initializers.random_normal(mean=0.0, stddev=0.1))
#     prev_z = act_func(prev_z @ prev_W + prev_b)

# y = prev_z
# correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
# accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
# train_step = tf.train.GradientDescentOptimizer(lr).minimize(cross_entropy)

# # Train
# sess = tf.Session()
# sess.run(tf.global_variables_initializer())
# train_accs = np.zeros((training_steps // 100))
# test_accs = np.zeros((training_steps // 100))
